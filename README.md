# ppcc
ppcc Is the Polyplex Content Compiler, it compiles files into .ppc files for use in polyplex.

### How To
Run `ppcc -c (files)` or `ppcc --convert (files)` to convert normal files into ppc packages.
This tool only packages textures currently, everything else will be packaged down into a RAW content type.
